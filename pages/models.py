from django.db import models

# Create your models here.
class OurTeam(models.Model):
    name        = models.CharField(max_length=30)
    img         = models.ImageField(upload_to='pics')
    discription = models.TextField()
    designation = models.CharField(default=None,max_length=40)

class ContactUs(models.Model):
    name        = models.CharField(max_length=30)
    email       = models.EmailField(max_length=30)
    subject     = models.CharField(max_length=60)
    Message     = models.TextField()
    
