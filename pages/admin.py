from django.contrib import admin
from .models import OurTeam, ContactUs

# Register your models here.
admin.site.register(OurTeam)
admin.site.register(ContactUs)
