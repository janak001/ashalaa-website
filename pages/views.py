from django.shortcuts import render
from .models import ContactUs, OurTeam

# Create your views here.
def index_page(request):
    # contactus = ContactUs.objects.all()
    ourteam   = OurTeam.objects.all()

    return render(request,'index.html', { 'ourteam':ourteam })
